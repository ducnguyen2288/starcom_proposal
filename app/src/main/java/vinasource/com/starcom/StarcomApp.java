package vinasource.com.starcom;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;

/**
 * Created by user on 5/30/16.
 */

public class StarcomApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//        Calendar.getInstance().setFirstDayOfWeek(Calendar.MONDAY);
//        Calendar.getInstance().setMinimalDaysInFirstWeek(4);

    }
}

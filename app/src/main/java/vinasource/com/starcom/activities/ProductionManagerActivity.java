package vinasource.com.starcom.activities;

import vinasource.com.starcom.R;
import vinasource.com.starcom.fragment.ProductMangerFragment;
import vinasource.com.starcom.manager.TransactionManager;

/**
 * Created by user on 5/25/16.
 */

public class ProductionManagerActivity extends BaseActivity {

    ProductMangerFragment productMangerFragment;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void setupViews() {
        productMangerFragment = new ProductMangerFragment();
        TransactionManager.getInstance().replaceFragment(getSupportFragmentManager(), productMangerFragment);
    }
}

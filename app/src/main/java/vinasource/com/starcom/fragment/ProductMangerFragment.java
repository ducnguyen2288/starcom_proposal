package vinasource.com.starcom.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.Bind;
import vinasource.com.starcom.R;
import vinasource.com.starcom.Util.Constants;
import vinasource.com.starcom.adapter.ProductionManagerViewPagerAdapter;
import vinasource.com.starcom.presenter.areaZone.AreaZoneFragment;
import vinasource.com.starcom.presenter.unplacedLots.UnPlacedLotsFragment;

/**
 * Created by user on 5/26/16.
 */

public class ProductMangerFragment extends BaseFragment implements View.OnClickListener {
    @Bind(R.id.viewpager)
    ViewPager productionManagerViewPager;
    @Bind(R.id.button_area)
    AppCompatButton btnArea;
    @Bind(R.id.button_unplaced_lots)
    AppCompatButton btnUnplacedLots;
    @Bind(R.id.linearlayout_tabbar)
    LinearLayout lnTabBar;
    @Bind(R.id.button_about)
    AppCompatButton btnAbout;

    AreaZoneFragment areaZoneFragment;
    UnPlacedLotsFragment unPlacedLotsFragment;

    ProductionManagerViewPagerAdapter areaSpaceViewPagerAdapter;
    private final int TAB_AREA = 0;
    private final int TAB_UNPLACED_LOTS = 1;
    private int currentTab = TAB_AREA;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        areaZoneFragment = new AreaZoneFragment();
        unPlacedLotsFragment = new UnPlacedLotsFragment();
        areaSpaceViewPagerAdapter = new ProductionManagerViewPagerAdapter(getChildFragmentManager());
        areaSpaceViewPagerAdapter.addFragment(areaZoneFragment);
        areaSpaceViewPagerAdapter.addFragment(unPlacedLotsFragment);
    }

    @Override
    public int getLayoutId() {
        return R.layout.production_manager_fragment;
    }

    @Override
    public void setupView() {
        btnArea.setOnClickListener(this);
        btnUnplacedLots.setOnClickListener(this);
        btnAbout.setOnClickListener(this);

        productionManagerViewPager.setAdapter(areaSpaceViewPagerAdapter);

        productionManagerViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateCurrentTab(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        selectAreaTab();

    }

    private void selectUnPlacedLotsTab() {
        updateCurrentTab(TAB_UNPLACED_LOTS);

    }

    private void selectAreaTab() {
        updateCurrentTab(TAB_AREA);
    }

    public void updateCurrentTab(int currentTab) {
        this.currentTab = currentTab;
        for (int i = 0; i < lnTabBar.getChildCount(); i++) {
            View childView = lnTabBar.getChildAt(i);
            if (childView instanceof AppCompatButton) {
                childView.setSelected(false);
                childView.setEnabled(true);
                ((AppCompatButton) childView).setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }


        }

        switch (currentTab) {
            case TAB_AREA:
                btnArea.setSelected(true);
                btnArea.setEnabled(false);
                btnArea.setTextColor(ContextCompat.getColor(getContext(), R.color.color_white));
                break;
            case TAB_UNPLACED_LOTS:
                btnUnplacedLots.setSelected(true);
                btnUnplacedLots.setEnabled(false);
                btnUnplacedLots.setTextColor(ContextCompat.getColor(getContext(), R.color.color_white));
                break;
            default:
                break;
        }

        productionManagerViewPager.setCurrentItem(currentTab);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_area:
                selectAreaTab();
                break;
            case R.id.button_unplaced_lots:
                selectUnPlacedLotsTab();
                break;
            case R.id.button_about:
                Toast.makeText(getActivity().getApplicationContext(), "Build date: " + Constants.BUILD_NUMBER, Toast.LENGTH_LONG).show();
                break;

        }
    }
}

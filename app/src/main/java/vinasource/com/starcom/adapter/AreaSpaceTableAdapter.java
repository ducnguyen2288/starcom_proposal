package vinasource.com.starcom.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

import vinasource.com.starcom.R;
import vinasource.com.starcom.objects.AreaSpaceUtil;
import vinasource.com.starcom.objects.AreaWeek;

public class AreaSpaceTableAdapter<T> extends BaseTableAdapter {

    private final static int WIDTH_DIP = 110;
    private final static int HEIGHT_DIP = 32;

    private final Context context;

    private AreaSpaceUtil[][] table;

    private final int width;
    private final int height;
    private final int FIRST_HEADER = 0;
    private final int FIRST_BODY = 1;
    private final int BODY = 2;
    private final int HEADER = 3;
    private final int MAX_ITEM_VIEW_COUNT = 4;


    public AreaSpaceTableAdapter(Context context) {
        this.context = context;
        Resources r = context.getResources();

//        width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, WIDTH_DIP, r.getDisplayMetrics()));
//        height = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, HEIGHT_DIP, r.getDisplayMetrics()));
        width = context.getResources().getDimensionPixelSize(R.dimen.table_width);
        height = context.getResources().getDimensionPixelSize(R.dimen.table_height);


    }

    public void setInformation(T[][] table) {
        this.table = (AreaSpaceUtil[][]) table;
        notifyDataSetChanged();
    }

    @Override
    public int getRowCount() {
        if(table == null){
            return 0;
        }
        return table.length - 1;
    }

    @Override
    public int getColumnCount() {
        if(table == null){
            return  0;
        }
        return table[0].length - 1;
    }

    @Override
    public View getView(final int row, final int column, View convertView, ViewGroup parent) {
        if(table != null){
//            Log.d(AreaSpaceTableAdapter.class.getSimpleName(), "Row: " + row + ". Column: " + column + ". Value: " + table[row + 1][column + 1]);
            switch (getItemViewType(row, column)) {
                case FIRST_HEADER:
                    convertView = getFirstHeader(row, column, convertView, parent);
                    break;
                case FIRST_BODY:
                    convertView = getFirstBody(row, column, convertView, parent);
                    break;
                case HEADER:
                    convertView = getHeader(row, column, convertView, parent);

                    break;
                //Default : body view
                default:
                {
                    convertView = getBody(row, column, convertView, parent);
                }

            }
        }else {
            convertView = new TextView(context);
        }

        return convertView;
    }


    private View getFirstHeader(final int row, final int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.first_header_view, parent, false);
        }
        TextView textView = ((TextView) convertView.findViewById(R.id.text1));
        textView.setText(table[row + 1][column + 1].getValue().getSpaceArea());
        return convertView;
    }

    private View getHeader(final int row, final int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.header_table_view, parent, false);
        }
        TextView textView = ((TextView) convertView.findViewById(R.id.header_name));
        textView.setText(table[row + 1][column + 1].getValue().getSpaceArea());
        return convertView;
    }

    private View getFirstBody(final int row, final int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.first_body_view, parent, false);
        }
        final AreaSpaceUtil areaSpaceUtil = table[row + 1][column + 1];

        TextView textView = ((TextView) convertView.findViewById(R.id.text1));
        CheckBox checkBoxSelection = (CheckBox) convertView.findViewById(R.id.checkbox_selection);
        checkBoxSelection.setChecked(areaSpaceUtil.isSelected());
        checkBoxSelection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                table[row + 1][column + 1].setSelected(isChecked);
            }
        });

        String areaName = areaSpaceUtil.getValue().getSpaceArea();
        textView.setText(areaName);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Clicked: " + areaSpaceUtil.getValue().getSpaceArea(), Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    private View getBody(final int row, final int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.body_view, parent, false);
        }
        final AreaSpaceUtil areaSpaceUtil = table[row + 1][column + 1];
        AreaWeek week = areaSpaceUtil.getAreaWeeks()[column];
        double consumedSpace = week.getConsumedSpace();
        final double freeSpace = week.getCapacity() - consumedSpace;


        TextView textViewFreeSpace = ((TextView) convertView.findViewById(R.id.free_space));
        TextView textViewConsumedSpace = ((TextView) convertView.findViewById(R.id.consumed_space));
        RelativeLayout rlContainer = (RelativeLayout) convertView.findViewById(R.id.container);

        textViewFreeSpace.setText(String.valueOf(freeSpace));
        textViewConsumedSpace.setText(String.valueOf(consumedSpace));

        Log.d(AreaSpaceTableAdapter.class.getSimpleName(),"AreaSpace: " + areaSpaceUtil.getValue().getSpaceArea() +
                " . AccumCost: " + week.getAccumCost() + " . Capacity: " + week.getCapacity() + " . ConsumedSpace: " + week.getConsumedSpace() +
                " . FreeSpace: " + week.getFreeSpace());

        Drawable bgColor;
        if(freeSpace ==0) {
            bgColor = ContextCompat.getDrawable(context, R.drawable.bg_button_white);
        }else if(freeSpace > 0){
            bgColor = ContextCompat.getDrawable(context, R.drawable.bg_body_view_green);
        }else {
            bgColor = ContextCompat.getDrawable(context, R.drawable.bg_body_view_pink);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            rlContainer.setBackground(bgColor);
        }else {
            rlContainer.setBackgroundDrawable(bgColor);
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Clicked: " + freeSpace, Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    @Override
    public int getHeight(int row) {
        if(row != -1){
            return context.getResources().getDimensionPixelSize(R.dimen.table_body_height);
        }
        return height;
    }

    @Override
    public int getWidth(int column) {
        if(column == -1){
            return  context.getResources().getDimensionPixelSize(R.dimen.table_first_body_width);
        }
        return width;
    }

    @Override
    public int getItemViewType(int row, int column) {
        final int itemViewType;
        if (row == -1 && column == -1) {
            itemViewType = FIRST_HEADER;
        } else if (column == -1) {
            itemViewType = FIRST_BODY;
        } else if (row == -1) {
            itemViewType = HEADER;
        } else {
            itemViewType = BODY;
        }

        return itemViewType;
    }

    @Override
    public int getViewTypeCount() {
        return MAX_ITEM_VIEW_COUNT;
    }
}

package vinasource.com.starcom.presenter.areaZone;

import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import vinasource.com.starcom.database.DbHelper;
import vinasource.com.starcom.R;
import vinasource.com.starcom.Util.DateHelper;
import vinasource.com.starcom.objects.AreaSpace;
import vinasource.com.starcom.objects.AreaSpaceCapacity;
import vinasource.com.starcom.objects.AreaSpaceUtil;
import vinasource.com.starcom.objects.DateArray;
import vinasource.com.starcom.objects.LotAction;
import vinasource.com.starcom.objects.LotSpace;
import vinasource.com.starcom.Util.StarcomHelper;

/**
 * Created by user on 5/25/16.
 */

public class AreaZonePresenterImpl implements AreaZonePresenter {
    AreaZoneView areaZoneView;
    CompositeSubscription compositeSubscription;
    DbHelper dbHelper;

    public AreaZonePresenterImpl(AreaZoneView areaZoneView){
        this.areaZoneView = areaZoneView;
        compositeSubscription = new CompositeSubscription();
        dbHelper = new DbHelper(this.areaZoneView.getContext());
    }

    @Override
    public void insertDataToLocalDB(final int resourceId) {
        areaZoneView.showLoading();
        Subscription subscription = Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    int insertCount = dbHelper.insertFromFile(areaZoneView.getContext(), resourceId);
                    subscriber.onNext(insertCount);

                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

                subscriber.onCompleted();

            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        getAllCapacities();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        areaZoneView.hideLoading();
                        Toast.makeText(areaZoneView.getContext(), "Insert data failure. " + throwable.toString(), Toast.LENGTH_SHORT).show();
                        Log.d(AreaZonePresenterImpl.class.getSimpleName(), "Insert data failure. " + throwable.toString());
                        getAllCapacities();
                    }
                }, new Action0() {
                    @Override
                    public void call() {
                        areaZoneView.hideLoading();
                    }
                });

        compositeSubscription.add(subscription);


    }

    @Override
    public void getAllCapacities() {
        ArrayList<AreaSpaceCapacity> areaSpaceCapacities = StarcomHelper.getAllCapacities(dbHelper);
        ArrayList<LotSpace> lotSpaces = StarcomHelper.getAllLotSpaces(dbHelper);
        ArrayList<LotAction> lotActions = StarcomHelper.getAllLotAction(dbHelper);
        if(areaSpaceCapacities.size() > 0){
            final List<AreaSpaceUtil> areaSpaceUtils =  StarcomHelper.computeCapacity(areaSpaceCapacities, lotSpaces, lotActions);
            int maxRow = areaSpaceUtils.size() + 1;
            int maxColumn = areaSpaceUtils.get(0).getAreaWeeks().length + 1;
            AreaSpaceUtil[][] spaceUtils = new AreaSpaceUtil[maxRow][maxColumn];
            for (int i = 0; i < maxRow; i++) {
                for (int j = 0; j < maxColumn; j++) {
                    if(i == 0){
                        AreaSpace areaSpace = new AreaSpace(0,0);
                        if(j == 0){
                            areaSpace.setSpaceArea(areaZoneView.getContext().getString(R.string.zone_area));
                        }else {
                            String time = DateHelper.convertUTCToLocalTimeInYearWeek(areaSpaceUtils.get(i).getAreaWeeks()[j-1].getSpaceDate().getTime());
                            areaSpace.setSpaceArea(time);
                        }

                        //Hard code date array - just for test
                        String week11Time = "2016-03-07";
                        Calendar calendar = DateHelper.convertStringToCalendar(week11Time, DateHelper.RFC_USA_6);
                        Date begDate = calendar.getTime();
                        DateArray dateArray = new DateArray(begDate, 8);

                        AreaSpaceUtil areaSpaceUtil = new AreaSpaceUtil(areaSpace, dateArray);
                        spaceUtils[i][j] = areaSpaceUtil;
                    }else {
                        spaceUtils[i][j] = areaSpaceUtils.get(i - 1);
                    }
                }
            }

            areaZoneView.bindAreaCapacities(spaceUtils);
        }

        areaZoneView.hideLoading();
    }

    @Override
    public void onDestroy() {
        if(compositeSubscription != null && !compositeSubscription.isUnsubscribed()){
            compositeSubscription.unsubscribe();
        }
        areaZoneView = null;
    }
}

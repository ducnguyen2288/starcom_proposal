package vinasource.com.starcom.presenter.areaZone;

import android.content.Context;

import vinasource.com.starcom.objects.AreaSpaceUtil;

/**
 * Created by user on 5/25/16.
 */

public interface AreaZoneView {
    void showLoading();
    void hideLoading();
    void bindAreaCapacities(AreaSpaceUtil[][] spaceUtils);
    Context getContext();
}

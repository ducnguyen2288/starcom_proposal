package vinasource.com.starcom.presenter.areaZone;

/**
 * Created by user on 5/25/16.
 */

public interface AreaZonePresenter {
    void insertDataToLocalDB(int resourceId);
    void getAllCapacities();
    void onDestroy();
}

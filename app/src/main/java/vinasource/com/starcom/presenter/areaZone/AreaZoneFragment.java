package vinasource.com.starcom.presenter.areaZone;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.inqbarna.tablefixheaders.TableFixHeaders;

import butterknife.Bind;
import vinasource.com.starcom.R;
import vinasource.com.starcom.adapter.AreaSpaceTableAdapter;
import vinasource.com.starcom.fragment.BaseFragment;
import vinasource.com.starcom.objects.AreaSpaceUtil;

/**
 * Created by user on 5/25/16.
 */

public class AreaZoneFragment extends BaseFragment implements AreaZoneView{
    @Bind(R.id.table)
    TableFixHeaders tableFixHeadersAreaSpace;

    AreaSpaceTableAdapter<AreaSpaceUtil> areaSpaceTableAdapter;
    AreaZonePresenter areaZonePresenter;
    private AlertDialog mAlertDialog;


    @Override
    public int getLayoutId() {
        return R.layout.area_zone_fragment;
    }

    @Override
    public void setupView() {
        areaSpaceTableAdapter = new AreaSpaceTableAdapter<AreaSpaceUtil>(getActivity());
        tableFixHeadersAreaSpace.setAdapter(areaSpaceTableAdapter);
        areaZonePresenter = new AreaZonePresenterImpl(this);
        areaZonePresenter.insertDataToLocalDB(R.raw.starcominventoryspacingdb);
    }

    @Override
    public void showLoading() {
        initAlertDialog();
        mAlertDialog.show();
    }

    @Override
    public void hideLoading() {
        if(!getActivity().isFinishing()){
            mAlertDialog.dismiss();
        }
    }

    @Override
    public void bindAreaCapacities(AreaSpaceUtil[][] spaceUtils) {
        areaSpaceTableAdapter.setInformation(spaceUtils);
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    private void initAlertDialog(){
        if(mAlertDialog == null){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Inserting data...Please wait.");
            builder.setCancelable(false);
            mAlertDialog = builder.create();
        }
    }

    @Override
    public void onDestroyView() {
        areaZonePresenter.onDestroy();
        super.onDestroyView();

    }
}

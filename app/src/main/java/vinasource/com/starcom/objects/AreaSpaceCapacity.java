package vinasource.com.starcom.objects;

import java.util.Date;
import java.util.List;

/**
 * Created by pauls on 5/11/2016.
 */
public class AreaSpaceCapacity {
	private int spaceAreaId;
	private String spaceArea;                            // short name for area
	private String zone;                            // concatenated hierarchical parents, below facility
	private int facilityId;
	private String facility;
	private int spaceTypeId;            // Since these will be static, does it work to carry both?
	private String spaceType;                //
	private Date asOfDate;
	private int capacity;
	private String unitOfMeasure;        // SqFeet/Linear/Units
	private List<SpaceAttribute> spaceAttributes;    // id/code: ZERO to MANY. Items that do not require any attributes can go in zero attributes

	public int getSpaceAreaId() {
		return spaceAreaId;
	}

	public void setSpaceAreaId(int spaceAreaId) {
		this.spaceAreaId = spaceAreaId;
	}

	public String getSpaceArea() {
		return spaceArea;
	}

	public void setSpaceArea(String spaceArea) {
		this.spaceArea = spaceArea;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public int getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public int getSpaceTypeId() {
		return spaceTypeId;
	}

	public void setSpaceTypeId(int spaceTypeId) {
		this.spaceTypeId = spaceTypeId;
	}

	public String getSpaceType() {
		return spaceType;
	}

	public void setSpaceType(String spaceType) {
		this.spaceType = spaceType;
	}

	public Date getAsOfDate() {
		return asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	public List<SpaceAttribute> getSpaceAttributes() {
		return spaceAttributes;
	}

	public void setSpaceAttributes(List<SpaceAttribute> spaceAttributes) {
		this.spaceAttributes = spaceAttributes;
	}
}

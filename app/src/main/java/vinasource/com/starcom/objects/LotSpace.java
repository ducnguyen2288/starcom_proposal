package vinasource.com.starcom.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 *
 * @author pauls Copyright Starcom Computer Corp 2016
 *
 *         LotSpace represents each InventoryLot (if planted) or Planting (if not yet planted). Thus either inventoryLotId or plantingId
 *         must have a value but not both. Each LotSpace is built from one or more LotLevel, which represents each lot over one or more
 *         Spacing Levels (as the plant grows it may require more space). The array of LotWeek represents this InventoryLot over the course
 *         of each week (as specified by the user).
 */
public class LotSpace {
	private int id;
	private int inventoryLotId;
	private int plantingId;
	private int productionLotId;  // Usually the id printed on the tag
	private Date plantDate;        // display YY:WW
	private Date readyDate;        // display YY:WW
	private Date expiryDate;        // display YY:WW
	private String category;          // Always Item Category
	private String itemDescription;  // Either PlantItem or InventoryItem
	private String fset;              // ForecastSet (designated for a specific customer), generally abbrev Set.
	private boolean isPlanted;        // if FALSE display in Planting tab
	private int facilityId;        // perhaps not displayed? If areaId == facilityId, then display in Facility tab
	private int areaId;            // Either blank (zero) or the assigned Area (display from table)
	private String area;              // Display value
	private int qty;              // Number of plants
	private int spaceAreaId;      // Could be a parent of physical area
	private String spaceArea;        // Display value
	private int totalWeeks;        // from planting to expiration
	private int spaceProfileId;    // foreign key to SpaceProfile.
	private String spaceProfile;      // Display value
	private List<LotLevel> lotLevels;        // Each SpaceLevel that this Lot grows through

	//
	public LotSpace() {
		lotLevels = new ArrayList<LotLevel>();
	}

	public int getInventoryLotId() {
		return inventoryLotId;
	}

	public void setInventoryLotId(int inventoryLotId) {
		this.inventoryLotId = inventoryLotId;
	}

	public int getPlantingId() {
		return plantingId;
	}

	public void setPlantingId(int plantingId) {
		this.plantingId = plantingId;
	}

	public int getProductionLotId() {
		return productionLotId;
	}

	public void setProductionLotId(int productionLotId) {
		this.productionLotId = productionLotId;
	}

	public Date getPlantDate() {
		return plantDate;
	}

	public void setPlantDate(Date plantDate) {
		this.plantDate = plantDate;
	}

	public Date getReadyDate() {
		return readyDate;
	}

	public void setReadyDate(Date readyDate) {
		this.readyDate = readyDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getFset() {
		return fset;
	}

	public void setFset(String fset) {
		this.fset = fset;
	}

	public boolean isPlanted() {
		return isPlanted;
	}

	public void setPlanted(boolean isPlanted) {
		this.isPlanted = isPlanted;
	}

	public int getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public int getSpaceAreaId() {
		return spaceAreaId;
	}

	public void setSpaceAreaId(int spaceAreaId) {
		this.spaceAreaId = spaceAreaId;
	}

	public int getTotalWeeks() {
		return totalWeeks;
	}

	public void setTotalWeeks(int totalWeeks) {
		this.totalWeeks = totalWeeks;
	}

	public int getSpaceProfileId() {
		return spaceProfileId;
	}

	public void setSpaceProfileId(int spaceProfileId) {
		this.spaceProfileId = spaceProfileId;
	}

	public String getSpaceProfile() {
		return spaceProfile;
	}

	public void setSpaceProfile(String spaceProfile) {
		this.spaceProfile = spaceProfile;
	}

	public String getSpaceArea() {
		return spaceArea;
	}

	public void setSpaceArea(String spaceArea) {
		this.spaceArea = spaceArea;
	}

	public List<LotLevel> getLotLevels() {
		return lotLevels;
	}

	public void setLotLevels(List<LotLevel> lotLevels) {
		this.lotLevels = lotLevels;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
}

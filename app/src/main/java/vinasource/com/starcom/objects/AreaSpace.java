package vinasource.com.starcom.objects;
/*
 * Copyright 2016 Starcom Computer Corp
 * @author
 * pauls Each object represents a Spacing Area plus SpaceType (which could be a parent to many Inventory Areas)
 */
public class AreaSpace {
	private int			spaceTypeId;	// This area is for this type of space
	private String	spaceType;		// Display value
	private int			spaceAreaId;	// Spacing Area Id
	private String	spaceArea = "";		// Display value
	private String	zone;					// Concatenation of spaceArea's parents
	private String	facility;			// Display value of top area which is facility

	public AreaSpace(int spaceTypeId, int spaceAreaId) {
		this.spaceTypeId = spaceTypeId;
		this.spaceAreaId = spaceAreaId;
	}

	public int getSpaceAreaId() {
		return spaceAreaId;
	}

	public void setSpaceAreaId(int areaId) {
		this.spaceAreaId = areaId;
	}

	public String getSpaceArea() {
		return spaceArea;
	}

	public void setSpaceArea(String area) {
		this.spaceArea = area;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getSpaceType() {
		return spaceType;
	}

	public void setSpaceType(String spaceType) {
		this.spaceType = spaceType;
	}

	public int getSpaceTypeId() {
		return spaceTypeId;
	}

	public void setSpaceTypeId(int spaceTypeId) {
		this.spaceTypeId = spaceTypeId;
	}
}

package vinasource.com.starcom.objects;

/**
 * Created by pauls on 5/11/2016.
 */
public class SpaceAttribute {
    private int id;
    private String attribute;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

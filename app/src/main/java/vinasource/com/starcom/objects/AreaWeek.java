package vinasource.com.starcom.objects;

import java.util.Date;

/**
 * Copyright 2016 Starcom Computer Corp
 * @author pauls
 * Each Area/SpaceType, for each Week in specified date range
 *
 */
public class AreaWeek {
	//private int			areaSpaceId;
	private Date		spaceDate;
	private int			capacity;
	private double	consumedSpace;
	private double	freeSpace;
	private double	accumCost;

	public AreaWeek( Date spaceDate) {
		//this.areaSpaceId = areaSpaceId;
		this.spaceDate = spaceDate;
	}

//	public int getAreaSpaceId() {
//		return areaSpaceId;
//	}
//
//	public void setAreaSpaceId(int areaSpaceId) {
//		this.areaSpaceId = areaSpaceId;
//	}

	public Date getSpaceDate() {
		return spaceDate;
	}

	public void setSpaceDate(Date spaceDate) {
		this.spaceDate = spaceDate;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public double getConsumedSpace() {
		return consumedSpace;
	}

	public void setConsumedSpace(double consumedSpace) {
		this.consumedSpace = consumedSpace;
	}

	public double getFreeSpace() {
		return freeSpace;
	}

	public void setFreeSpace(double freeSpace) {
		this.freeSpace = freeSpace;
	}

	public double getAccumCost() {
		return accumCost;
	}

	public void setAccumCost(double accumCost) {
		this.accumCost = accumCost;
	}
}

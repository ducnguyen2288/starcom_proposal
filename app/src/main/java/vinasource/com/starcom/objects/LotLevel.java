package vinasource.com.starcom.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * Copyright 2016 Starcom Computer Corp
 * Each SpaceLevel that a Lot can grow through
 */
public class LotLevel {
	private int id;                // as a parent to spaceAttributes
	private int lotSpaceId;        // my parent
	private double spaceRequired;
	private int spaceWeeks;        // Number of Weeks at this level
	private int spaceTypeId;      // The type of space (Bench, Floor, Hanging, etc)
	private String spaceType;        // // Display value
	private int level;            // 1,2...
	private double costPerSFW;        // Standard Cost per Square Foot Week
	private Date spaceBegDate;      // This level begins on this date
	private Date spaceEndDate;      // This level ends the day before this date
	private double spacingSize;      // The amount of space each unit occupies
	private List<SpaceLevelAttribute> spaceAttributes;  // NYI: id/code: ZERO to MANY. Required/suggested attributes for this item

	public LotLevel() {
		this.spaceAttributes = new ArrayList<SpaceLevelAttribute>();
	}

	public double getSpaceRequired() {
		return spaceRequired;
	}

	public void setSpaceRequired(double spaceRequired) {
		this.spaceRequired = spaceRequired;
	}

	public int getSpaceWeeks() {
		return spaceWeeks;
	}

	public void setSpaceWeeks(int spaceWeeks) {
		this.spaceWeeks = spaceWeeks;
	}

//	public int getTotalWeeks() {
//		return totalWeeks;
//	}
//
//	public void setTotalWeeks(int totalWeeks) {
//		this.totalWeeks = totalWeeks;
//	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Date getSpaceBegDate() {
		return spaceBegDate;
	}

	public void setSpaceBegDate(Date spaceBegDate) {
		this.spaceBegDate = spaceBegDate;
	}

	public Date getSpaceEndDate() {
		return spaceEndDate;
	}

	public void setSpaceEndDate(Date spaceEndDate) {
		this.spaceEndDate = spaceEndDate;
	}

	public double getSpacingSize() {
		return spacingSize;
	}

	public void setSpacingSize(double spacingSize) {
		this.spacingSize = spacingSize;
	}

	public int getSpaceTypeId() {
		return spaceTypeId;
	}

	public void setSpaceTypeId(int spaceTypeId) {
		this.spaceTypeId = spaceTypeId;
	}

	public List<SpaceLevelAttribute> getSpaceAttributes() {
		return spaceAttributes;
	}

	public void setSpaceAttributes(List<SpaceLevelAttribute> spaceAttributes) {
		this.spaceAttributes = spaceAttributes;
	}

	public String getSpaceType() {
		return spaceType;
	}

	public void setSpaceType(String spaceType) {
		this.spaceType = spaceType;
	}

	public int getLotSpaceId() {
		return lotSpaceId;
	}

	public void setLotSpaceId(int lotSpaceId) {
		this.lotSpaceId = lotSpaceId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCostPerSFW() {
		return costPerSFW;
	}

	public void setCostPerSFW(double costPerSFW) {
		this.costPerSFW = costPerSFW;
	}
}

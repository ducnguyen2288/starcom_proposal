package vinasource.com.starcom.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * Copyright 2016 Starcom Computer Corp
 * Wraps one LotSpace and computes its spacing across weeks
 */
public class LotSpaceUtil {
	private LotSpace				value;
	private List<LotAction>	fromLotActions;	// Moves scheduled to take place to this Lot in the future
	private List<LotAction>	intoLotActions;	// Moves scheduled to take place to this Lot in the future
	private LotWeek[]				lotWeeks;				// Spacing data for each week of specified date range
	// NYI private List<LotAssigned>					assigned;					// lotId/date/qty/spaceRequired  // Not Yet Implemented
	// NYI private List<SpaceLevelAttribute>	spaceAttributes;	// id/code: ZERO to MANY. Required/suggested attributes for this item

	public LotSpaceUtil(LotSpace lotSpace) {
		this.value = lotSpace;
		init();
	}

	private void init() {
		this.fromLotActions = new ArrayList<LotAction>();
		this.intoLotActions = new ArrayList<LotAction>();
		// NYI this.spaceAttributes = new ArrayList<SpaceLevelAttribute>();
	}

	public void compute(DateArray dateArray, AreaSpaceUtil asu) {// for each LotLevel, how does it apply to the LotWeeks
		freshWeeks(dateArray);
		//
		double accumCost = 0.00;
		for (LotLevel ll : value.getLotLevels()) {
			if (ll.getSpaceTypeId() != asu.getValue().getSpaceTypeId()) {
				continue; // Probably another level is in this spaceType
			}
			Date incrDate = new Date(ll.getSpaceBegDate().getTime());
			while (ll.getSpaceEndDate().after(incrDate)) {
				Integer index = dateArray.dateToIndex(incrDate);
				// whether before start weeks or not, accumulate cost
				accumCost += ll.getSpaceRequired() * ll.getCostPerSFW(); // TODO what about spacing factor (non-SF)
				if (index != null ) {
					LotWeek lotWeek = lotWeeks[index];
					lotWeek.setConsumedQty(value.getQty());
					lotWeek.setSpacingSize(ll.getSpacingSize());
					lotWeek.setConsumedSpace(ll.getSpaceRequired());
					lotWeek.setAccumCost(accumCost);
				}
				incrDate = dateArray.nextWeek(incrDate);
			}
		}
		for (LotAction la : fromLotActions) {
			Date incrDate = new Date(la.getActionDate().getTime());
			while (incrDate.before(value.getExpiryDate())) {
				Integer index = dateArray.dateToIndex(incrDate);
				if (index != null) {
					LotWeek lotWeek = lotWeeks[index];
					if (la.getFromAreaId() == value.getAreaId()) { // This action is moving OUT OF this area
						lotWeek.setConsumedQty((int) Math.round(lotWeek.getConsumedQty() - la.getQty()));
						lotWeek.setConsumedSpace(lotWeek.getConsumedSpace() - (la.getQty() * lotWeek.getSpacingSize())); // moving from this area
					} else {
						//ystem.out.println("WRONG");
						// else this lotAction is in the wrong place
					}
				}
				incrDate = dateArray.nextWeek(incrDate);
			}
		}
		for (LotAction la : intoLotActions) {
			Date incrDate = new Date(la.getActionDate().getTime());
			while (incrDate.before(value.getExpiryDate())) {
				Integer index = dateArray.dateToIndex(incrDate);
				if (index != null) {
					LotWeek lotWeek = lotWeeks[index];
					if (la.getToAreaId() == value.getAreaId()) { // moving INTO this area
						lotWeek.setConsumedQty((int) Math.round(lotWeek.getConsumedQty() + la.getQty()));
						lotWeek.setConsumedSpace(lotWeek.getConsumedSpace() + (la.getQty() * lotWeek.getSpacingSize())); // moving into this area
					} else {
						//ystem.out.println("WRONG");
						// else this lotAction is in the wrong place
					}
				}
				incrDate = dateArray.nextWeek(incrDate);
			}
		}
	}

	private void freshWeeks(DateArray dateArray) {
		lotWeeks = new LotWeek[dateArray.getWeeks()];
		for (int i = 0; i < lotWeeks.length; i++) {
			lotWeeks[i] = new LotWeek(dateArray.indexToDate(i));
		}
	}

	public LotSpaceUtil clone() {
		LotSpace ls = new LotSpace();
		ls.setArea(value.getArea());
		ls.setAreaId(value.getAreaId());
		ls.setCategory(value.getCategory());
		ls.setExpiryDate(new Date(value.getExpiryDate().getTime()));
		ls.setFacilityId(value.getFacilityId());
		ls.setFset(value.getFset());
		ls.setId(value.getId());
		ls.setInventoryLotId(value.getInventoryLotId());
		ls.setItemDescription(value.getItemDescription());
		ls.setLotLevels(new ArrayList<LotLevel>()); //TODO
		for (LotLevel ll : value.getLotLevels()) {
			ls.getLotLevels().add(cloneLevel(ll));
		}
		ls.setPlantDate(new Date(value.getPlantDate().getTime()));
		ls.setPlanted(value.isPlanted());
		ls.setPlantingId(value.getPlantingId());
		ls.setProductionLotId(value.getProductionLotId());
		ls.setQty(value.getQty());
		ls.setReadyDate(new Date(value.getReadyDate().getTime()));
		ls.setSpaceArea(value.getSpaceArea());
		ls.setSpaceAreaId(value.getSpaceAreaId());
		ls.setSpaceProfile(value.getSpaceProfile());
		ls.setSpaceProfileId(value.getSpaceProfileId());
		ls.setTotalWeeks(value.getTotalWeeks());
		LotSpaceUtil u = new LotSpaceUtil(ls);
		return u;
	}

	private LotLevel cloneLevel(LotLevel s) {
		LotLevel ll = new LotLevel();
		ll.setId(s.getId());
		ll.setLevel(s.getLevel());
		ll.setLotSpaceId(s.getLotSpaceId());
		ll.setSpaceAttributes(s.getSpaceAttributes()); // These can be the same objects?
		ll.setSpaceBegDate(s.getSpaceBegDate());
		ll.setSpaceEndDate(s.getSpaceEndDate());
		ll.setSpaceRequired(s.getSpaceRequired());
		ll.setSpaceType(s.getSpaceType());
		ll.setSpaceTypeId(s.getSpaceTypeId());
		ll.setSpaceWeeks(s.getSpaceWeeks());
		ll.setSpacingSize(s.getSpacingSize());
		ll.setCostPerSFW(s.getCostPerSFW());
		return ll;
	}

	public LotSpace getValue() {
		return value;
	}

	public LotWeek[] getLotWeeks() {
		return lotWeeks;
	}

	public List<LotAction> getFromLotActions() {
		return fromLotActions;
	}

	public List<LotAction> getIntoLotActions() {
		return intoLotActions;
	}
}

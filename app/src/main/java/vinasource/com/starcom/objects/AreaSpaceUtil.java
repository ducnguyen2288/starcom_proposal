package vinasource.com.starcom.objects;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Copyright 2016 Starcom Computer Cort
 * @author pauls
 * Wraps one AreaSpace, plus related objects.
 *
 */
public class AreaSpaceUtil {
	private AreaSpace										value;
	private Map<Integer, LotSpaceUtil>	inventoryLots;	// Keyed on InventoryLotId
	private Map<Integer, LotSpaceUtil>	plantingLots;		// unplanted, keyed on PlantingId
	private List<LotSpaceUtil>					lotSpacings;		// list of both inventoryLots and plantingLots
	private List<AreaSpaceCapacity>			capacityList;		// This area/spacetype's capacity, over time (generally just one)
	private AreaWeek[]									areaWeeks;			// Each week in specified date range. This is the data to display in the main screen, per area
	private DateArray dateArray;			// Tracks each week of the date range
	private boolean selected;
	public AreaSpaceUtil(AreaSpace value, DateArray dateArray) {
		this.value = value;
		this.dateArray = dateArray;
		this.capacityList = new ArrayList<AreaSpaceCapacity>(); // not really necessary since capacity can be computed once
		this.inventoryLots = new HashMap<Integer, LotSpaceUtil>();
		this.plantingLots = new HashMap<Integer, LotSpaceUtil>();
		this.lotSpacings = new ArrayList<>();
		newWeeks();
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public AreaSpaceUtil(){}

	public void addAreaSpaceCapacity(AreaSpaceCapacity asc) {
		capacityList.add(asc);
		/*
		 * AreaCapacity has asOfDate, but no endDate. So setting the weekly capacities for all weeks after the AreaCapacity.asOfDate could
		 * correctly overwrite an earlier asOfDate's capacity with a later asOfDate's capacity. Note that the list must be pre-sorted by
		 * asOfDate
		 */
		Date eachDate = dateArray.firstRelevantDate(asc.getAsOfDate()); // the later of asOfDate or begin date of array
		while (dateArray.getEndDate().after(eachDate)) { // from asOfDate to last week of time span
			Integer index = dateArray.dateToIndex(eachDate);
			if (index != null) {
				areaWeeks[index].setCapacity(asc.getCapacity()); // could possibly be an overwrite of previous asOfDate
			}
			eachDate = dateArray.nextWeek(eachDate);
		}
	}

	/*
	 * Each LotLevel must find its LotSpaceUtil to be later computed across the LotSpaceUtil.LotWeeks
	 */
	public void addLotSpace(LotSpace lotSpace) {
		if (lotSpace.getInventoryLotId() != 0) {
			LotSpaceUtil lotSpaceU = inventoryLots.get(lotSpace.getInventoryLotId());
			if (lotSpaceU == null) {
				lotSpaceU = new LotSpaceUtil(lotSpace);
				inventoryLots.put(lotSpaceU.getValue().getInventoryLotId(), lotSpaceU);
				lotSpacings.add(lotSpaceU);
			}
		} else if (lotSpace.getPlantingId() != 0) {
			LotSpaceUtil lotSpaceU = plantingLots.get(lotSpace.getPlantingId());
			if (lotSpaceU == null) {
				lotSpaceU = new LotSpaceUtil(lotSpace);
				plantingLots.put(lotSpaceU.getValue().getPlantingId(), lotSpaceU);
				lotSpacings.add(lotSpaceU);
			}
		}
		//		lotSpacing.getLotLevels().add(lotSpace);
	}

	/*
	 * When receiving LotActions, the AreaSpace to which it is being moved to generally will not have that lot in the to-area, so must clone
	 * from the from-area and add to this AreaSpace.
	 */
	public void addLotSpaceUtil(LotSpaceUtil ls) {
		if (ls.getValue().getInventoryLotId() != 0) {
			inventoryLots.put(ls.getValue().getInventoryLotId(), ls);
		} else {
			plantingLots.put(ls.getValue().getPlantingId(), ls);
		}
		lotSpacings.add(ls);
	}

	public LotSpaceUtil getLotSpaceUtil(int inventoryLotId, int plantingLotId) {
		if (inventoryLotId != 0) {
			return getInventoryLotSpaceUtil(inventoryLotId);
		} else if (plantingLotId != 0) {
			return getPlantingLotSpaceUtil(plantingLotId);
		} else {
			return null; // should not happen
		}
	}

	public LotSpaceUtil getInventoryLotSpaceUtil(int id) {
		return inventoryLots.get(id);
	}

	public LotSpaceUtil getPlantingLotSpaceUtil(int id) {
		return plantingLots.get(id);
	}

	public void compute() {
		freshWeeks();
		//
		if (true) {
			if (lotSpacings.size() == 0) { // To report by Item, there must be at least one Item-level object
				// ystem.out.println("adding dummy to " + value.getSpaceArea() + value.getSpaceType());
				LotSpaceUtil dummy = new LotSpaceUtil(new LotSpace());
				lotSpacings.add(dummy);
			}
			for (LotSpaceUtil lotSpacing : lotSpacings) {
				lotSpacing.compute(dateArray, this);
				// Accumulate each lot in this area up to this area's weeks
				for (int i = 0; i < areaWeeks.length; i++) {
					areaWeeks[i].setConsumedSpace(areaWeeks[i].getConsumedSpace() + lotSpacing.getLotWeeks()[i].getConsumedSpace());
					areaWeeks[i].setAccumCost(areaWeeks[i].getConsumedSpace() + lotSpacing.getLotWeeks()[i].getAccumCost());
				}
			}
		} else { // On a mobile device, there may be no need to build all of the detail objects until they need them
			computeAreaSpaceDirectly();
		}
	}

	private void freshWeeks() {
		for (AreaWeek aw : areaWeeks) {
			aw.setAccumCost(0);
			aw.setConsumedSpace(0);
			aw.setFreeSpace(0);
		}
	}

	private void newWeeks() {
		areaWeeks = new AreaWeek[dateArray.getWeeks()];
		for (int i = 0; i < dateArray.getWeeks(); i++) {
			areaWeeks[i] = new AreaWeek(dateArray.indexToDate(i));
		}
	}

	/**
	 * If it takes too much memory to build all of the detail objects, then sum up to the AreaSpace.AreaWeek objects, the AreaSpace.AreaWeeks
	 * could be computed directly. (Then the detail objects would have to be computed as each is displayed)
	 */
	private void computeAreaSpaceDirectly() {
		// This is untested code
		//		for (LotLevel ls : lotSpacings) { // for each LotLevel, how does it apply to the AreaWeeks
		//			Date incrDate = new Date(ls.getSpaceBegDate().getTime());
		//			while (ls.getSpaceEndDate().after(incrDate)) {
		//				Integer index = dateArray.dateToIndex(incrDate);
		//				if (index != null) {
		//					AreaWeek areaWeek = areaWeeks[index];
		//					areaWeek.setConsumedSpace(areaWeek.getConsumedSpace() + ls.getSpaceRequired());
		//				}
		//				incrDate = dateArray.nextWeek(incrDate);
		//			}
		//			for (LotAction la : ls.getLotActions()) {
		//				incrDate = new Date(la.getActionDate().getTime());
		//				while (ls.getSpaceEndDate().after(incrDate)) {
		//					Integer index = dateArray.dateToIndex(incrDate);
		//					if (index != null) {
		//						AreaWeek areaWeek = areaWeeks[index];
		//						if (la.getFromAreaId() == ls.getAreaId()) {
		//							areaWeek.setConsumedSpace(areaWeek.getConsumedSpace() - ls.getSpaceRequired()); // moving from this area
		//						} else {
		//							areaWeek.setConsumedSpace(areaWeek.getConsumedSpace() + ls.getSpaceRequired()); // moving into this area
		//						}
		//						areaWeek.setFreeSpace(areaWeek.getCapacity() - areaWeek.getConsumedSpace());
		//					}
		//					incrDate = dateArray.nextWeek(incrDate);
		//				}
		//			}
		//		}
	}

	// Not sure if this will eventually be needed.
	//	public LotSpace cloneLotSpace(LotSpace ls) {
	//		LotSpace c = new LotSpace();
	//		c.setId(ls.getId());
	//		c.setInventoryLotId(ls.getInventoryLotId());
	//		c.setPlantingId(ls.getPlantingId());
	//		c.setProductionLotId(ls.getProductionLotId());
	//		c.setPlantDate(ls.getPlantDate());
	//		c.setReadyDate(ls.getReadyDate());
	//		c.setExpiryDate(ls.getExpiryDate());
	//		c.setCategory(ls.getCategory());
	//		c.setItemDescription(ls.getItemDescription());
	//		c.setFset(ls.getFset());
	//		c.setPlanted(ls.isPlanted());
	//		c.setFacilityId(ls.getFacilityId());
	//		c.setAreaId(ls.getAreaId());
	//		c.setSpaceAreaId(ls.getSpaceAreaId());
	//		c.setSpaceArea(ls.getSpaceArea());
	//		c.setSpacingSize(ls.getSpacingSize());
	//		c.setTotalWeeks(ls.getTotalWeeks());
	//		c.setCostPerSFW(ls.getCostPerSFW());
	//		c.setSpaceProfileId(ls.getSpaceProfileId());
	//		c.setSpaceProfile(ls.getSpaceProfile());
	//		return c;
	//	}
	//
	public AreaSpace getValue() {
		return value;
	}

	public List<LotSpaceUtil> getLotSpacings() {
		return lotSpacings;
	}

	public AreaWeek[] getAreaWeeks() {
		return areaWeeks;
	}
}

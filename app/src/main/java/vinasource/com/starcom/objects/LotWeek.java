package vinasource.com.starcom.objects;
import java.util.Date;

public class LotWeek {
	private Date		spaceDate;
	private int			consumedQty;
	private double	spacingSize;
	private double	consumedSpace;
	private int			assignedQty;
	private double	accumCost;

	public LotWeek(Date date) {
		setSpaceDate(date);
	}

	public Date getSpaceDate() {
		return spaceDate;
	}

	public void setSpaceDate(Date spaceDate) {
		this.spaceDate = spaceDate;
	}

	public int getConsumedQty() {
		return consumedQty;
	}

	public void setConsumedQty(int consumedQty) {
		this.consumedQty = consumedQty;
	}

	public double getSpacingSize() {
		return spacingSize;
	}

	public void setSpacingSize(double spacingSize) {
		this.spacingSize = spacingSize;
	}

	public double getConsumedSpace() {
		return consumedSpace;
	}

	public void setConsumedSpace(double consumedSpace) {
		this.consumedSpace = consumedSpace;
	}

	public int getAssignedQty() {
		return assignedQty;
	}

	public void setAssignedQty(int assignedQty) {
		this.assignedQty = assignedQty;
	}

	public double getAccumCost() {
		return accumCost;
	}

	public void setAccumCost(double accumCost) {
		this.accumCost = accumCost;
	}
}

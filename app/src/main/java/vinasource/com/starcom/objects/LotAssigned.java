package vinasource.com.starcom.objects;

import java.util.Date;

public class LotAssigned {
	private int			lotSpacingId;	// parent LotLevelWeb, just a fabricated id on download
	private Date		shipDate;
	private int			assignedQty;
	private double	spaceRequired;

	public int getLotLevelId() {
		return lotSpacingId;
	}

	public void setLotLevelId(int lotSpacingId) {
		this.lotSpacingId = lotSpacingId;
	}

	public Date getShipDate() {
		return shipDate;
	}

	public void setShipDate(Date shipDate) {
		this.shipDate = shipDate;
	}

	public int getAssignedQty() {
		return assignedQty;
	}

	public void setAssignedQty(int assignedQty) {
		this.assignedQty = assignedQty;
	}

	public double getSpaceRequired() {
		return spaceRequired;
	}

	public void setSpaceRequired(double spaceRequired) {
		this.spaceRequired = spaceRequired;
	}
}

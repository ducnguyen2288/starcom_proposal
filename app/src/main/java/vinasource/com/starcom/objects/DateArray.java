package vinasource.com.starcom.objects;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.time.DateUtils.isSameDay;

public class DateArray {
	private Date								begDate, endDate;
	private Map<Date, Integer>	dateIndexMap	= new HashMap<Date, Integer>();
	private Map<Integer, Date>	indexDateMap	= new HashMap<Integer, Date>();
	private int									weeks;

	public DateArray(Date begDate, int weeks) {
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(begDate);
//		calendar.set(Calendar.DAY_OF_WEEK, 2);
//		begDate = calendar.getTime();

		setBegDate(begDate);
		setWeeks(weeks);
		Calendar cal = Calendar.getInstance();
		cal.setTime(begDate);
		for (int i = 0; i < weeks; i++) {
			Date eachWeekDate = cal.getTime();
			dateIndexMap.put(eachWeekDate, i);
			indexDateMap.put(i, eachWeekDate);
			cal.add(Calendar.DAY_OF_YEAR, 7);
		}
		this.setEndDate(cal.getTime()); // the last date
	}

	public Date firstRelevantDate(Date asOfDate) {
		if (asOfDate.after(begDate)) {
			return asOfDate;
		} else {
			return begDate;
		}
	}

	public Date nextWeek(Date thisWeek) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(thisWeek);
		cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + 7);
		Date result = cal.getTime();
		return result;
	}

	public Integer dateToIndex(Date date) {
		date = resetTimeToMidnight(date);
		return dateIndexMap.get(date);
	}

	private Date resetTimeToMidnight(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		date = calendar.getTime();
		return date;
	}

	public Integer dateToIndex2(Date date){
		int position = -1;
		for (int i = 0; i < indexDateMap.size(); i++) {
			Date dateInMap = indexDateMap.get(i);
			boolean result = isSameDay(date, dateInMap);
			if(result){
				position = i;
				break;
			}
		}

		return (position != -1) ? position : null;
	}

	public Date indexToDate(int i) {
		return indexDateMap.get(i);
	}

	public Date getBegDate() {
		return begDate;
	}

	public void setBegDate(Date begDate) {
		this.begDate = begDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getWeeks() {
		return weeks;
	}

	public void setWeeks(int weeks) {
		this.weeks = weeks;
	}
}

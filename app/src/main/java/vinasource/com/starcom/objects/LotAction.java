package vinasource.com.starcom.objects;

import java.util.Date;

/*
 * Each Transaction to be scheduled to be performed on an InventoryLot or Planting
 */
public class LotAction {
	private int			id;
	private int			action_status;		// NYI
	private int			inventoryLotId;
	private int			plantingId;
	private Date actionDate;				// The date that this action is scheduled for
	private int			actionTypeId;			// From a list of Constants (assume a MOVE for now)
	private int			fromAreaId;				// from inventory area, not necessarily spacing area
	private int			fromSpaceAreaId;
	private int			toAreaId;					// move to inventory area, not necessarily spacing area
	private int			toSpaceAreaId;
	private int			fromSpaceTypeId;	//
	private int			toSpaceTypeId;		// They can move a lot to a different spaceType
	private double	qty;							// The qty to move; not necessarily the whole lot.

	public int getInventoryLotId() {
		return inventoryLotId;
	}

	public void setInventoryLotId(int inventoryLotId) {
		this.inventoryLotId = inventoryLotId;
	}

	public int getPlantingId() {
		return plantingId;
	}

	public void setPlantingId(int plantingId) {
		this.plantingId = plantingId;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public int getActionTypeId() {
		return actionTypeId;
	}

	public void setActionTypeId(int actionTypeId) {
		this.actionTypeId = actionTypeId;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAction_status() {
		return action_status;
	}

	public void setAction_status(int action_status) {
		this.action_status = action_status;
	}

	public int getFromAreaId() {
		return fromAreaId;
	}

	public void setFromAreaId(int fromAreaId) {
		this.fromAreaId = fromAreaId;
	}

	public int getToAreaId() {
		return toAreaId;
	}

	public void setToAreaId(int toAreaId) {
		this.toAreaId = toAreaId;
	}

	public int getFromSpaceTypeId() {
		return fromSpaceTypeId;
	}

	public void setFromSpaceTypeId(int fromSpaceTypeId) {
		this.fromSpaceTypeId = fromSpaceTypeId;
	}

	public int getToSpaceTypeId() {
		return toSpaceTypeId;
	}

	public void setToSpaceTypeId(int toSpaceTypeId) {
		this.toSpaceTypeId = toSpaceTypeId;
	}

	public int getFromSpaceAreaId() {
		return fromSpaceAreaId;
	}

	public void setFromSpaceAreaId(int fromSpaceAreaId) {
		this.fromSpaceAreaId = fromSpaceAreaId;
	}

	public int getToSpaceAreaId() {
		return toSpaceAreaId;
	}

	public void setToSpaceAreaId(int toSpaceAreaId) {
		this.toSpaceAreaId = toSpaceAreaId;
	}
}

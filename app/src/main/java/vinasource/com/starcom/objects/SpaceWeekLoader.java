package vinasource.com.starcom.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/*
 * Copyright 2016 Starcom Computer Corp
 * @author pauls
 * This class loads data into each respective AreaSpace, and calls for the computed results.
 */
public class SpaceWeekLoader {
	private DateArray dateArray;
	private AreaSpaceUtilMap areaSpaceMap = new AreaSpaceUtilMap();

	public SpaceWeekLoader(DateArray dateArray) {
		this.dateArray = dateArray;
	}

	public void loadCapacities(List<AreaSpaceCapacity> caps) {
		for (AreaSpaceCapacity asc : caps) {
			AreaSpaceUtil areaSpace = areaSpaceMap.confirmAreaSpace(asc.getSpaceTypeId(), asc.getSpaceType(), asc.getSpaceAreaId(),
					asc.getSpaceArea(), asc.getZone(), asc.getFacility());
			areaSpace.addAreaSpaceCapacity(asc);
		}
	}

	public void loadLots(List<LotSpace> lots) {
		for (LotSpace ls : lots) {
			for (LotLevel ll : ls.getLotLevels()) { // Each Level can ask for a different SpaceType, even though it is the Lot that is assigned to an area
				// If at this point the areaSpace doesn't exist, it probably means that the lot is assigned to the facility, which has no capacity of its own
				AreaSpaceUtil as = areaSpaceMap.confirmAreaSpace(ll.getSpaceTypeId(), ll.getSpaceType(), ls.getSpaceAreaId(), ls.getSpaceArea(), "",
						ls.getArea());
				as.addLotSpace(ls);
			}
		}
	}

	public void loadActions(List<LotAction> laList) {
		for (LotAction la : laList) {
			AreaSpaceUtil fromAS = areaSpaceMap.confirmAreaSpace(la.getFromSpaceTypeId(), la.getFromSpaceAreaId());
			LotSpaceUtil fromLS = fromAS.getLotSpaceUtil(la.getInventoryLotId(), la.getPlantingId());
			fromLS.getFromLotActions().add(la);
			AreaSpaceUtil intoAS = areaSpaceMap.confirmAreaSpace(la.getToSpaceTypeId(), la.getToSpaceAreaId());
			LotSpaceUtil intoLS = intoAS.getLotSpaceUtil(la.getInventoryLotId(), la.getPlantingId());
			if (intoLS == null) {
				intoLS = fromLS.clone();
				intoLS.getValue().setAreaId(la.getToAreaId());
				intoAS.addLotSpaceUtil(intoLS);
			}
			intoLS.getIntoLotActions().add(la);
		}
	}

	/*
	 * Can be called repeatedly, since it recalcs from scratch
	 *
	 * @return List of AreaSpace (wrapped in AreaSpaceUtil), which owns AreaWeek array plus contributing LotSpaces.
	 */
	public List<AreaSpaceUtil> compute() {
		List<AreaSpaceUtil> results = new ArrayList<AreaSpaceUtil>();
		for (Map<Integer, AreaSpaceUtil> areaMap : areaSpaceMap.values()) { // by SpaceType
			for (AreaSpaceUtil areaSpace : areaMap.values()) { // by Area
				areaSpace.compute();
				results.add(areaSpace);
			}
		}
		return results;
	}


	public List<AreaSpaceUtil> compute2() {

		Map<String, AreaSpaceUtil> results = new TreeMap<String, AreaSpaceUtil>();

		for (Map<Integer, AreaSpaceUtil> areaMap : areaSpaceMap.values()) { // by SpaceType

			for (AreaSpaceUtil areaSpace : areaMap.values()) { // by Area

				areaSpace.compute();

				String key = areaSpace.getValue().getZone() + ":" + areaSpace.getValue().getSpaceArea() + ":" + areaSpace.getValue().getSpaceType();

				results.put(key, areaSpace);

			}

		}
		return new ArrayList<AreaSpaceUtil>(results.values());
	}


	//--------------------------------------------------------------

	/*
	 * This Map is actually two levels. The top map is by SpaceTypeId, which holds Maps by AreaId
	 */
	private class AreaSpaceUtilMap extends HashMap<Integer, Map<Integer, AreaSpaceUtil>> {
		public AreaSpaceUtil confirmAreaSpace(int spaceTypeId, String spaceType, int spaceAreaId, String area, String zone, String facility) {
			AreaSpaceUtil areaSpaceU = confirmAreaSpace(spaceTypeId, spaceAreaId);
			areaSpaceU.getValue().setSpaceType(spaceType);
			areaSpaceU.getValue().setSpaceArea(area);
			areaSpaceU.getValue().setZone(zone);
			areaSpaceU.getValue().setFacility(facility);
			return areaSpaceU;
		}

		public AreaSpaceUtil confirmAreaSpace(int spaceTypeId, int spaceAreaId) {
			Map<Integer, AreaSpaceUtil> spaceTypeMap = get(spaceTypeId);
			if (spaceTypeMap == null) {
				spaceTypeMap = new HashMap<Integer, AreaSpaceUtil>();
				put(spaceTypeId, spaceTypeMap);
			}
			AreaSpaceUtil areaSpaceU = spaceTypeMap.get(spaceAreaId);
			if (areaSpaceU == null) {
				areaSpaceU = new AreaSpaceUtil(new AreaSpace(spaceTypeId, spaceAreaId), dateArray);
				spaceTypeMap.put(spaceAreaId, areaSpaceU);
			}
			return areaSpaceU;
		}
	}
}

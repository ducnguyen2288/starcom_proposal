package vinasource.com.starcom.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import vinasource.com.starcom.Util.Constants;
import vinasource.com.starcom.objects.AreaSpaceCapacity;
import vinasource.com.starcom.objects.LotAction;
import vinasource.com.starcom.objects.LotLevel;
import vinasource.com.starcom.objects.LotSpace;

/**
 * Created by user on 5/25/16.
 */

public class DbHelper extends SQLiteOpenHelper {
    SQLiteDatabase db ;
    public DbHelper(Context context) {
        super(context, Constants.DB_NAME, null, 1);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public int insertFromFile(Context context, int resourceId) throws IOException {
        // Reseting Counter
        int result = 0;
        try{


            // Open the resource
            InputStream insertsStream = context.getResources().openRawResource(resourceId);
            BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));

            // Iterate through lines (assuming each insert has its own line and theres no other stuff)
            while (insertReader.ready()) {
                String insertStmt = insertReader.readLine();
                Log.d(DbHelper.class.getSimpleName(), "insertStmt: "  + insertStmt);
                db.execSQL(insertStmt);
                result++;
            }
            insertReader.close();
        }catch (Exception e){
            Log.e(DbHelper.class.getSimpleName(), "insertFromFile(): " + e.toString());
        }

        return result;
    }

    public ArrayList<AreaSpaceCapacity> getAllCapacities(){
        ArrayList<AreaSpaceCapacity> areaSpaceCapacities = new ArrayList<>();
        openDatabase();

        Cursor cursor = null;

        String query = "SELECT * FROM area_space_capacity ORDER BY space_area ASC";

        try{
            cursor = db.rawQuery(query, null);

            if(cursor != null){
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    AreaSpaceCapacity areaSpaceCapacity = mapDataArea(cursor);
                    areaSpaceCapacities.add(areaSpaceCapacity);
                }
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            Log.e(DbHelper.class.getSimpleName(), "getAllCapacitys(): " + e.toString() );
        }finally {
            if(cursor != null){
                cursor.close();
            }
//            this.close();
        }

        return areaSpaceCapacities;
    }

    public ArrayList<LotSpace> getAllLotSpaces(){
        ArrayList<LotSpace> lotSpaces = new ArrayList<>();
        openDatabase();

        Cursor cursor = null;

        String query = "SELECT * FROM lot_space";

        try{
            cursor = db.rawQuery(query, null);

            if(cursor != null){
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    LotSpace lotSpace = mapDataLotSpace(cursor);
                    lotSpaces.add(lotSpace);
                }
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            Log.e(DbHelper.class.getSimpleName(), "getAllLotSpaces(): " + e.toString() );
        }finally {
            if(cursor != null){
                cursor.close();
            }
//            this.close();
        }

        for (int i = 0; i < lotSpaces.size() ; i++) {
            LotSpace lotSpace = lotSpaces.get(i);
            ArrayList<LotLevel> lotLevels = getLotLevelsBySpaceId(lotSpace.getId());
            lotSpace.setLotLevels(lotLevels);
        }

        return lotSpaces;
    }

    public ArrayList<LotLevel> getLotLevelsBySpaceId(int spaceId){
        ArrayList<LotLevel> lotLevels = new ArrayList<>();
        openDatabase();

        Cursor cursor = null;

        String query = "SELECT * FROM lot_level WHERE lot_space_id = " + spaceId;

        try{
            cursor = db.rawQuery(query, null);

            if(cursor != null){
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    LotLevel lotLevel = mapDataLotLevel(cursor);
                    lotLevels.add(lotLevel);
                }
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            Log.e(DbHelper.class.getSimpleName(), "getLotLevelsBySpaceId(): " + e.toString() );
        }finally {
            if(cursor != null){
                cursor.close();
            }
//            this.close();
        }
        return lotLevels;
    }

    public ArrayList<LotAction> getAllLotAction(){
        ArrayList<LotAction> lotSpaces = new ArrayList<>();
        openDatabase();

        Cursor cursor = null;

        String query = "SELECT * FROM lot_action";

        try{
            cursor = db.rawQuery(query, null);

            if(cursor != null){
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    LotAction lotAction = mapDataLotAction(cursor);
                    lotSpaces.add(lotAction);
                }
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            Log.e(DbHelper.class.getSimpleName(), "getAllLotAction(): " + e.toString() );
        }finally {
            if(cursor != null){
                cursor.close();
            }
//            this.close();
        }

        return lotSpaces;
    }

    private AreaSpaceCapacity mapDataArea(Cursor result){

        AreaSpaceCapacity areaSpaceCapacity = new AreaSpaceCapacity();

        areaSpaceCapacity.setSpaceAreaId(result.getInt(result.getColumnIndex("space_area_id")));
        areaSpaceCapacity.setSpaceArea(result.getString(result.getColumnIndex("space_area")));
        areaSpaceCapacity.setZone(result.getString(result.getColumnIndex("zone")));
        areaSpaceCapacity.setFacilityId(result.getInt(result.getColumnIndex("facility_id")));
        areaSpaceCapacity.setFacility(result.getString(result.getColumnIndex("facility")));
        areaSpaceCapacity.setSpaceTypeId(result.getInt(result.getColumnIndex("space_type_id")));
        areaSpaceCapacity.setSpaceType(result.getString(result.getColumnIndex("space_type")));



        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("as_of_date")));
        areaSpaceCapacity.setAsOfDate(calendar.getTime());
        areaSpaceCapacity.setCapacity(result.getInt(result.getColumnIndex("capacity")));
        areaSpaceCapacity.setUnitOfMeasure(result.getString(result.getColumnIndex("unit_of_measure")));


        return areaSpaceCapacity;
    }

    private LotSpace mapDataLotSpace(Cursor result){

        LotSpace lotSpace = new LotSpace();
        lotSpace.setId(result.getInt(result.getColumnIndex("id")));
        lotSpace.setInventoryLotId(result.getInt(result.getColumnIndex("inventory_lot_id")));
        lotSpace.setPlantingId(result.getInt(result.getColumnIndex("planting_id")));
        lotSpace.setProductionLotId(result.getInt(result.getColumnIndex("production_lot_id")));
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("plant_date")));
        lotSpace.setPlantDate(calendar.getTime());

        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("ready_date")));
        lotSpace.setReadyDate(calendar.getTime());

        if(lotSpace.getId() == 65 || lotSpace.getId() == 64){
            int a = 0;
            a += 1;
        }
        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("expiry_date")));
        lotSpace.setExpiryDate(calendar.getTime());

        lotSpace.setCategory(result.getString(result.getColumnIndex("category")));

        lotSpace.setItemDescription(result.getString(result.getColumnIndex("item_description")));
        lotSpace.setFset(result.getString(result.getColumnIndex("fset")));
        lotSpace.setPlanted(result.getInt(result.getColumnIndex("is_planted")) > 0 ? true : false);
        lotSpace.setFacilityId(result.getInt(result.getColumnIndex("facility_id")));
        lotSpace.setAreaId(result.getInt(result.getColumnIndex("area_id")));
        lotSpace.setArea(result.getString(result.getColumnIndex("area")));
        lotSpace.setSpaceAreaId(result.getInt(result.getColumnIndex("space_area_id")));
        lotSpace.setSpaceArea(result.getString(result.getColumnIndex("space_area")));
        lotSpace.setTotalWeeks(result.getInt(result.getColumnIndex("total_weeks")));
        lotSpace.setSpaceProfileId(result.getInt(result.getColumnIndex("space_profile_id")));
        lotSpace.setSpaceProfile(result.getString(result.getColumnIndex("space_profile")));


        return lotSpace;
    }

    private LotAction mapDataLotAction(Cursor result){

        LotAction lotAction = new LotAction();
        lotAction.setId(result.getInt(result.getColumnIndex("id")));
        lotAction.setAction_status(result.getInt(result.getColumnIndex("action_status")));
        lotAction.setInventoryLotId(result.getInt(result.getColumnIndex("inventory_lot_id")));
        lotAction.setPlantingId(result.getInt(result.getColumnIndex("planting_id")));
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("action_date")));
        lotAction.setActionDate(calendar.getTime());

        lotAction.setActionTypeId(result.getInt(result.getColumnIndex("action_type_id")));
        lotAction.setFromAreaId(result.getInt(result.getColumnIndex("from_area_id")));
        lotAction.setFromSpaceAreaId(result.getInt(result.getColumnIndex("from_space_area_id")));
        lotAction.setToAreaId(result.getInt(result.getColumnIndex("to_area_id")));
        lotAction.setToSpaceAreaId(result.getInt(result.getColumnIndex("to_space_area_id")));
        lotAction.setFromSpaceTypeId(result.getInt(result.getColumnIndex("from_space_type_id")));
        lotAction.setQty(result.getDouble(result.getColumnIndex("qty")));


        return lotAction;
    }

    private LotLevel mapDataLotLevel(Cursor result){

        LotLevel lotLevel = new LotLevel();

        lotLevel.setId(result.getInt(result.getColumnIndex("id")));
        lotLevel.setLotSpaceId(result.getInt(result.getColumnIndex("lot_space_id")));
        lotLevel.setSpaceRequired(result.getDouble(result.getColumnIndex("space_required")));
        lotLevel.setSpaceWeeks(result.getInt(result.getColumnIndex("space_weeks")));
        lotLevel.setSpaceTypeId(result.getInt(result.getColumnIndex("space_type_id")));
        lotLevel.setSpaceType(result.getString(result.getColumnIndex("space_type")));
        lotLevel.setLevel(result.getInt(result.getColumnIndex("level")));
        lotLevel.setCostPerSFW(result.getDouble(result.getColumnIndex("cost_per_sfw")));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("space_beg_date")));
        lotLevel.setSpaceBegDate(calendar.getTime());

        calendar.setTimeInMillis(result.getLong(result.getColumnIndex("space_end_date")));
        lotLevel.setSpaceEndDate(calendar.getTime());

        lotLevel.setSpacingSize(result.getDouble(result.getColumnIndex("spacing_size")));

        return lotLevel;
    }

    private void closeDatabase(){
        db.close();
    }

    private void openDatabase() {
        if (db == null || !db.isOpen()) {
            db = getWritableDatabase();db.close();
        }
    }
}

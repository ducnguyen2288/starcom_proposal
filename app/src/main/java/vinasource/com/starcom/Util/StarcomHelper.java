package vinasource.com.starcom.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import vinasource.com.starcom.database.DbHelper;
import vinasource.com.starcom.objects.AreaSpaceCapacity;
import vinasource.com.starcom.objects.AreaSpaceUtil;
import vinasource.com.starcom.objects.DateArray;
import vinasource.com.starcom.objects.LotAction;
import vinasource.com.starcom.objects.LotSpace;
import vinasource.com.starcom.objects.SpaceWeekLoader;

/**
 * Created by user on 5/25/16.
 */

public class StarcomHelper {
    private static final String BEG_TIME = "2016-03-07";
    private static final int NUMBER_OF_WEEKS = 8;
    public static ArrayList<AreaSpaceCapacity> getAllCapacities(DbHelper dbHelper){
        ArrayList<AreaSpaceCapacity> areaSpaceCapacities = dbHelper.getAllCapacities();

        return areaSpaceCapacities;
    }

    public static ArrayList<LotSpace> getAllLotSpaces(DbHelper dbHelper){
        ArrayList<LotSpace> allLotSpaces = dbHelper.getAllLotSpaces();
        return allLotSpaces;
    }

    public static ArrayList<LotAction> getAllLotAction(DbHelper dbHelper){
        ArrayList<LotAction> lotActions = dbHelper.getAllLotAction();
        return lotActions;
    }

    public static List<AreaSpaceUtil> computeCapacity(ArrayList<AreaSpaceCapacity> areaSpaceCapacities, ArrayList<LotSpace> lotSpaces, ArrayList<LotAction> lotActions){

        Calendar calendar = DateHelper.convertStringToCalendar(BEG_TIME, DateHelper.RFC_USA_6);
        Date begDate = calendar.getTime();
        DateArray dateArray = new DateArray(begDate, NUMBER_OF_WEEKS);
        SpaceWeekLoader spaceWeekLoader = new SpaceWeekLoader(dateArray);

        spaceWeekLoader.loadCapacities(areaSpaceCapacities);
        spaceWeekLoader.loadLots(lotSpaces);
        spaceWeekLoader.loadActions(lotActions);

        List<AreaSpaceUtil> areaSpaceUtils = spaceWeekLoader.compute2();
        reorderAreaSpaceList(areaSpaceUtils);

        return areaSpaceUtils;
    }

    private static void reorderAreaSpaceList(List<AreaSpaceUtil> areaSpaceUtils){
        Collections.sort(areaSpaceUtils, new Comparator<AreaSpaceUtil>() {
            @Override
            public int compare(AreaSpaceUtil lhs, AreaSpaceUtil rhs) {
                return lhs.getValue().getSpaceArea().compareTo(rhs.getValue().getSpaceArea());
            }
        });
    }
}
